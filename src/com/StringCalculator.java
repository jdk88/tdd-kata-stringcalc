package com;

import java.util.Arrays;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringCalculator {

    public final static String COMMA_NEWLINE_DELIMITER = ",|\n";
    public final static String START_DELIMITER = "//";
    public final static String PARSING_ERROR = "Error while parsing the given string : %s, it doesn't match the generic delimiter";
    public final static String GENERIC_DELIMITER = "//(.*)\n(.*)";
    public final static String NEGATIVE_NUMBER_ERROR_MESSAGE = "The string contains negative numbers : %s";
    public final static int DEFAULT_DELIMITER_START_INDEX = 2;
    public final static int STRING_SPLIT_START_INDEX = 4;


    public int add(String str) {
        if(str.isEmpty()) return 0;
        Integer[] splitString = splitString(str);
        checkNegativeNumbers(splitString);
        return Arrays.stream(splitString)
                .reduce(Integer::sum)
                .orElse(0);
    }

    public Integer[] splitString(String str) {
        if (str.startsWith(START_DELIMITER)) {
            Matcher matcher = Pattern.compile(GENERIC_DELIMITER).matcher(str);
            if (matcher.matches()) {
                String defaultDelimiter = String.valueOf(str.charAt(DEFAULT_DELIMITER_START_INDEX));
                return stringToIntegerArray(str.substring(STRING_SPLIT_START_INDEX).split(defaultDelimiter));
            }
            throw new RuntimeException(String.format(PARSING_ERROR, str));
        }
        return stringToIntegerArray(str.split(COMMA_NEWLINE_DELIMITER));
    }

    public void checkNegativeNumbers(Integer[] nums) {
        StringBuilder negativeNumbers = new StringBuilder();
        Predicate<Integer> negativeNumberPredicate = n -> n < 0;
        Consumer<Integer> appendConsumer = (s) -> negativeNumbers.append(s).append(" ");

        Arrays.stream(nums)
                .filter(negativeNumberPredicate)
                .forEach(appendConsumer);

        if (!String.valueOf(negativeNumbers).isEmpty())
            throw new RuntimeException(String.format(NEGATIVE_NUMBER_ERROR_MESSAGE, negativeNumbers));
    }

    public Integer[] stringToIntegerArray(String[] intArray){
        return Arrays.stream(intArray).map(Integer::parseInt).toArray(Integer[]::new);
    }
}
