import com.StringCalculator;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import static org.junit.jupiter.api.Assertions.*;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class StringCalculatorTest {

    public static final String GENERIC_DELIMITER_ERROR_MESSAGE = "it doesn't match the generic delimiter";
    public static final String NEGATIVE_NUMBERS_ERROR_MESSAGE = "The string contains negative numbers";

    private StringCalculator stringCalculator;

    @BeforeAll
    public void setup() {
        stringCalculator = new StringCalculator();
    }

    @Test
    public void should_return_number(){
        assertTrue(Integer.valueOf(stringCalculator.add("1,2")) instanceof Integer);
    }

    @Test
    public void should_return_0_when_empty_string() {
        assertEquals(0, stringCalculator.add(""));
    }

    @Test
    public void should_sum_numbers_seperated_by_comma() {
        assertEquals(3, stringCalculator.add("1,2"));
        assertEquals(40, stringCalculator.add("17,23"));
        assertEquals(6, stringCalculator.add("1,2,3"));
        assertEquals(16, stringCalculator.add("1,2,3,10"));
    }

    @Test
    public void should_sum_numbers_seperated_by_newline() {
        assertEquals(3, stringCalculator.add("1\n2"));
        assertEquals(33, stringCalculator.add("15\n18"));
        assertEquals(6, stringCalculator.add("1\n2\n3"));
        assertEquals(16, stringCalculator.add("1\n2\n3\n10"));
    }

    @Test
    public void should_sum_numbers_separated_by_comma_or_newline() {
        assertEquals(8, stringCalculator.add("1,3\n4"));
        assertEquals(80, stringCalculator.add("10,30\n40"));
    }

    @Test
    public void should_throw_exception_when_having_successive_delimiters() {
        assertThrows(NumberFormatException.class, () -> stringCalculator.add("1,\n2"));
        assertThrows(NumberFormatException.class, () -> stringCalculator.add("1\n,2"));
    }

    @Test
    public void should_support_different_delimiters() {
        assertEquals(3, stringCalculator.add("//;\n1;2"));
    }

    @Test
    public void should_throw_exception_when_generic_delimiter_invalid() {
        Exception genericDelimiterExcption1 = assertThrows(RuntimeException.class, () -> stringCalculator.add("//1,\n\n2"));
        Exception genericDelimiterExcption2 = assertThrows(RuntimeException.class, () -> stringCalculator.add("//1,,,2"));
        assertTrue(genericDelimiterExcption1.getMessage().contains(GENERIC_DELIMITER_ERROR_MESSAGE));
        assertTrue(genericDelimiterExcption2.getMessage().contains(GENERIC_DELIMITER_ERROR_MESSAGE));
    }

    @Test
    void should_throw_exception_when_having_negative_numbers() {
        Exception negativeNumsException = assertThrows(RuntimeException.class, () -> stringCalculator.add("-1,2\n-3"));
        assertTrue(negativeNumsException.getMessage().contains(NEGATIVE_NUMBERS_ERROR_MESSAGE));
        assertTrue(negativeNumsException.getMessage().contains("-1 -3"));
    }

    @Test
    public void should_splitString_split_numbers_seperated_by_comma(){
        Integer[] result1 = stringCalculator.splitString("1,2");
        assertEquals(result1[0], 1);
        assertEquals(result1[1], 2);

        Integer[] result2 = stringCalculator.splitString("1,2,3");
        assertEquals(result2[0], 1);
        assertEquals(result2[1], 2);
        assertEquals(result2[2], 3);
    }

    @Test
    public void should_splitString_split_numbers_seprated_by_newline(){
        Integer[] result1 = stringCalculator.splitString("1\n2");
        assertEquals(result1[0], 1);
        assertEquals(result1[1], 2);

        Integer[] result2 = stringCalculator.splitString("1\n2\n3");
        assertEquals(result2[0], 1);
        assertEquals(result2[1], 2);
        assertEquals(result2[2], 3);
    }

    @Test
    public void should_splitString_throw_exception_when_having_successive_delimiters() {
        assertThrows(NumberFormatException.class, () -> stringCalculator.splitString("1,\n2"));
        assertThrows(NumberFormatException.class, () -> stringCalculator.splitString("1\n,2"));
    }

    @Test
    public void should_splitString_support_generic_delimiter(){
        Integer[] result1 = stringCalculator.splitString("//;\n1;2");
        assertEquals(result1[0], 1);
        assertEquals(result1[1], 2);
    }

    @Test
    public void should_splitString_throw_exception_when_generic_delimiter_invalid() {
        Exception genericDelimiterExcption1 = assertThrows(RuntimeException.class, () -> stringCalculator.splitString("//1,\n\n2"));
        Exception genericDelimiterExcption2 = assertThrows(RuntimeException.class, () -> stringCalculator.splitString("//1,,,2"));
        assertTrue(genericDelimiterExcption1.getMessage().contains(GENERIC_DELIMITER_ERROR_MESSAGE));
        assertTrue(genericDelimiterExcption2.getMessage().contains(GENERIC_DELIMITER_ERROR_MESSAGE));
    }

    @Test
    public void testStringtoIntArray(){
        String[] strings = {"1","2","3","4"};
        Integer[] ints = stringCalculator.stringToIntegerArray(strings);
        assertEquals(ints[0], 1);
        assertEquals(ints[1], 2);
        assertEquals(ints[2], 3);
        assertEquals(ints[3], 4);
    }

}
